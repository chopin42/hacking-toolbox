# Contribute

If you want to add your knowledge or just contribute to the project, you can do one of these things:

## What you can do

* **Submit an issue about a request of something to do in the future**
* **Submit an issue about a vulnerability or an error in the code**
* **Reformulate the tutorials to be more understandable by everyone**
* Create new scripts and tutorial to explain them. Notice that it is better to create a issue before doing it.
* **Share this project**
* Help other users who faces technical issues in the issue section and/or solve them using pull-requests
* Implement ideas in feature requests
* **Translate tutorials**
* Etc

## The rules you need to respect

* Do not make things that could be harmful to the user
* Always comment your code
* Explain in a detailed way your code in the tutorial
* Explain the usage of it in the tutorial as well as why you should consider this method instead of others. If you want to propose more methods, you can!
* If a warning is necessary, please add it at the top of your tutorial.
* If your project works only on one OS, specify it in the warning and/or in the title of the project.

## How to contribute

### To post an issue

1. Create an account on gitea.com
2. Go on the issue tab and search if your issue exist.
3. If not you can create a new one
4. In this issue describe your problem / request in detail and add the appropriate tag.
5. Discuss with other users about your changes

### To post a pull-request

1. Create an account on gitea.com
2. Search if it hasn't been done already
3. If not, you can propose an issue to discuss about it or directly skip this step
4. Click on the fork button to create your own copy
5. Modify the files online (or use git if you want to)
6. Go on the original project and click on the 'pull request tab' then create a new one
7. Describe your changes and publish your PR
8. Discuss with the other users about your changes
9. You will be notified if your changes are merged into the original repository.

#