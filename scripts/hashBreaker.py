#Import module hashlib as hl
import hashlib as hl

#Get the inputs
passHash = input('The hash to crack> ')
wordlistName = input('Filename> ')

#Open the wordlistName as wordlist
wordlist = open(wordlistName, 'r')

#Test all words of the wordlist
for word in wordlist:
    print('Trying ' + word + ' on ' + passHash)

    #Hash the word
    encWord = word.encode('utf-8')
    digest = hl.md5(encWord.strip()).hexdigest()

    #Compare to the digest
    if digest == passHash:
        print('Password has been found! The password is ' + word)
        quit()

print('The password has not been found in the dictionary')
