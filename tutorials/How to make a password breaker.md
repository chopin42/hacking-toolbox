# How to make a password breaker?

> Warning! This is just an example! Real password breakers like JhonTheRipper are much more useful in real cases, go check it out. A tutorial will be done about it in the future. BTW: The dictionary is too easy, just an example.

This is a very basic tutorial about breaking password and hashes.

To understand how that works let's start with a bit of theory:

## In theory, how does passwords are stored

Basically passwords of websites you visit are stored in *databases*. But they are not just added to the database in clear. Or they shouldn't because there are still some unsecured websites of course!

But they are first 'hashed'. The hash of a file is like the fingerprint of the file. It's a one way function.

Example: You put a fruit into the blender, once the fruit goes out you cannot reproduce the fruit to it's initial form. But you can still easily difference an apple from an orange by the color. It's the same thing here except that there is no blender, only maths.

| String in clear | Hash of the string (using md5)   |
| --------------- | -------------------------------- |
| apple           | 1f3870be274f6c49b3e31a0c6728957f |
| orange          | fe01d67a002dfa0f3ac084298142eccd |
| Apple           | 9f6290f4436e5a2351f12e03b6433c3c |
| appple          | 058744a996e8e703362bdec61e499e31 |

So that's what's stored into the database. It's seems secure except that it depends on the password!

If the password (the original string in clear) is `12345678` and this password is in the dictionary of a hacker (a dictionary or 'wordlist' is a list of the most widely used passwords) then this happens:

1. The hacker gets a database of password where yours is present
2. The hacker generate the hashes of all its dictionary
3. The hacker compares the hashes to yours
4. One hash matched to yours, the corresponding password is `12345678`
5. Because most people uses the same password everywhere, the hacker may be access to all your digital life.

## How it works in code

Now let's get into practice!

You can get access to the whole code [here](../scripts/hashBreaker.py)

To use this script you will also need a dictionary. You can find one [here](../assets/wordlist.txt)

Then start the following command:

```
python3 hashBreaker.py
```

For the hash, put one of the hash you'll find example: `1f3870be274f6c49b3e31a0c6728957f`

For the dictionary, insert the path to the dictionary or just the filename if it is in the same folder.

You should get something like this:

```shell
The hash to crack> 1f3870be274f6c49b3e31a0c6728957f
Filename> /home/snowcode/Programming/Hacking related/hacking-toolbox/assets/wordlist.txt
Trying apple
 on 1f3870be274f6c49b3e31a0c6728957f
Password has been found! The password is apple
```
