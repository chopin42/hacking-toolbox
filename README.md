# The simple hacking toolbox

This project has for objective to share tutorials and scripts for ethical hacking. These scripts will be mainly Python to be understandable, easy to use and universal.

The tutorials are just MD files.



## Usage

To use this project you just need to go into the 'tutorials' folder and select the tutorial that interest you. If the tutorial needs a script, then a script will be linked to it and the usage will be explained in the tutorial. You can of course also go into the scripts folder and have fun browsing the code.

## Installation

This project is **not** a software that means that it will not be something to install, it is just educational resources. To download them and have access without connection you can download it using [this link.](link here)

Or by using the following command if you are a git user:

```
git clone https://gitea.com/chopin42/hacking-toolbox
```

## Contribute

If you want to contribute, check [this file](./CONTRIBUTE.md)

## License informations

This project is under the GNU license, go into the [LICENSE file](./LICENSE) for more informations.







